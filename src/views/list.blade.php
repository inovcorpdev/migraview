@extends("todolist::app")

@section("content")
    <h4>Proposals</h4>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Date Time</th>
            <th>Title</th>
            <th class="text-right">Value</th>
        </tr>
        </thead>
        <tbody>
        @foreach($proposals as $proposal)
            <tr>
                <td>{{ $proposal->date_time }}</td>
                <td>{{ $proposal->title }}</td>
                <td class="text-right">{{ $proposal->value }}€</td>
            </tr>
        </tbody>
        @endforeach
    </table>
@endsection
