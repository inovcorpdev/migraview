<?php

namespace Inovcorp\Migraview;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inovcorp\Migraview\Models\Proposal;

class ProposalController extends Controller
{
    public function index()
    {
        $proposals = Proposal::all();
        return view('todolist::list', compact('proposals'));
    }
}
