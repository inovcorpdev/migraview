<?php

use Illuminate\Support\Facades\Route;

Route::get('/proposals', 'Inovcorp\Migraview\ProposalController@index');
