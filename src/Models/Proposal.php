<?php

namespace Inovcorp\Migraview\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_time',
        'title',
        'value'
    ];
}
